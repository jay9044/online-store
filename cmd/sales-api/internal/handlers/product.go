package handlers

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/jay9044/garagesale/internal/product"
	"github.com/jmoiron/sqlx"
)

//ProductService - has handler method for dealing with products
//instead of add db as an argument to the list function
type ProductService struct {
	DB *sqlx.DB
}

// list gives all product as a list
func (p *ProductService) List(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	list, err := product.List(p.DB)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Printf("Issue selecting query, error: %s", err)
		return
	}

	if err := json.NewEncoder(w).Encode(list); err != nil {
		product.ReturnError(w)
		panic(err)
	}
}
