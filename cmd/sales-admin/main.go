package main

import (
	"flag"
	"fmt"

	"os"

	"github.com/jay9044/garagesale/internal/platform/database"
	"github.com/jay9044/garagesale/internal/schema"
)

// go run ./cmd/sales-admin migrate
// go run ./cmd/sales-admin seed

func main() {

	// =========================================================================
	// Connecting to the DB

	db, err := database.Open()
	if err != nil {
		fmt.Printf("Issue connecting to db, error: %s", err)
		os.Exit(1)
	}

	defer db.Close()

	flag.Parse()

	//We return so function just executes specific function and not run the api
	//e.g go run . seed
	switch flag.Arg(0) {
	case "migrate":
		if err = schema.Migrate(db); err != nil {
			fmt.Printf("Issue Migrating, error: %s", err)
			os.Exit(1)
		}
		fmt.Println("Migrate successful")
		return
	case "seed":
		if err = schema.Seed(db); err != nil {
			fmt.Printf("Issue Seeding to db, error: %s", err)
			os.Exit(1)
		}
		fmt.Println("Seed successful")
		return
	}

}
