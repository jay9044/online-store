package database

import (
	"fmt"
	"net/url"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq" //register the postgres database/sql driver
)

//Open - Creates connection to DB
func Open() (*sqlx.DB, error) {
	q := url.Values{}
	q.Set("sslmode", "disable")
	q.Set("timezone", "utc")

	u := url.URL{
		Scheme:   "postgres",
		User:     url.UserPassword("postgres", "root"),
		Host:     "localhost",
		Path:     "postgres",
		RawQuery: q.Encode(),
	}

	fmt.Println(u.String())
	return sqlx.Open("postgres", u.String())
}
