package product

import (
	"encoding/json"
	"net/http"
	"time"

	"github.com/jmoiron/sqlx"
)

//Product - Items for sale
type Product struct {
	//the extra db tag is so fields match with whats in the db
	ID          string    `db:"product_id" json:"id,omitempty"`
	Name        string    `db:"name" json:"name,omitempty"`
	Price       float32   `db:"price" json:"price,omitempty"`
	Quantity    int       `db:"quantity" json:"quantity,omitempty"`
	Description string    `db:"description" json:"description,omitempty"`
	DateCreated time.Time `db:"date_created" json:"date_created,omitempty"`
	DateUpdated time.Time `db:"date_updated" json:"date_updated,omitempty"`
}

//Error - Basic error handling
type Error struct {
	Message    string `json:"message,omitempty"`
	StatusCode int    `json:"statuscode,omitempty"`
}

//List - select all product from db and return them
func List(db *sqlx.DB) ([]Product, error) {
	list := []Product{}

	//TODO - Not ideal - explicitly put all fields instead
	const query = `SELECT * FROM products`

	//select method is from sqlx package //does appending to slice also
	if err := db.Select(&list, query); err != nil {
		return nil, err
	}

	return list, nil
}

//ReturnError - basic error handling for invalid requests
func ReturnError(w http.ResponseWriter) {
	err := Error{}
	err.Message = "Invalid request"
	err.StatusCode = http.StatusNotFound
	json.NewEncoder(w).Encode(err)
}
